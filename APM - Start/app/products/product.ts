/**
 * In general, the interface is always a good a idea.
 * It provides type checking and intellisence to the editor.
 * 
 * It is a good a idea to use the prefix I, to distinguis the
 * interface from the bussines object.
 * 
 * The bussines object is not mandatory and in general it will
 * only make sense in case it performs some calculations, like the
 * calculate discount in this example
 * 
 */
export interface IProduct {
    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string;
    price: number;
    description: string;
    starRating: number;
    imageUrl: string;
}

export class Product implements IProduct {
    constructor(public productId: number,
                public productName: string,
                public productCode: string,
                public releaseDate: string,
                public price: number,
                public description: string,
                public starRating: number,
                public imageUrl: string) {}
    
    calculateDiscount(percent: number): number {
        return this.price - (this.price * percent / 100);
    }
}