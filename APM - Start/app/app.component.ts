import { Component } from '@angular/core';

@Component({
    selector: 'pm-app',
    template: `
    <div class="row">
        <pm-products></pm-products>
    </div>
    `
})
export class AppComponent {
    pageTitle: string = 'Acme Product Management';
}
